#!/bin/bash
set -e -o pipefail

jack_connect SuperCollider:out_1 "baudline:in_1"
jack_connect SuperCollider:out_2 "baudline:in_2"

jack_connect SuperCollider:out_1 "Simple Scope (4 channel):in1"
jack_connect SuperCollider:out_2 "Simple Scope (4 channel):in2"

jack_connect SuperCollider:out_1 "Spectr":in
jack_connect SuperCollider:out_2 "Spectr":in

jack_connect SuperCollider:out_1 "EBU R128 Meter":inL
jack_connect SuperCollider:out_2 "EBU R128 Meter":inR
