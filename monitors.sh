#!/bin/bash
set -e -o pipefail

declare -a procs=(
    "baudline_jack -jack -channels 2 -inconnect OFF -outconnect OFF"
    "x42-scope"
    "x42-spectr"
    "x42-meter"
)

pids=()

trap 'kill $(jobs -p)' EXIT

for i in "${!procs[@]}"; do
    eval "${procs[$i]} &"
    pids+=($!)
    sleep 0.5
done


# wait for all pids
for pid in ${pids[*]}; do
    wait $pid
done
