(ql:quickload :cl-audio-programming)
(in-package :audio)

(defsynth noiser ((out 0) (pan 0) (freq 440) (band-freq 0.5) (band-amp 10) (band-centre 100))
  (let* ((noise (pink-noise.ar))
         (band-flux (sin-osc.kr band-freq 0.0 band-amp band-centre))
         (bpf (bpf.ar noise band-flux (/ band-flux freq))))
    (out.ar out (pan2.ar bpf pan))))

(setf *synth* (synth 'noiser))
(ctrl *synth* :band-centre 140)
(ctrl *synth* :band-amp 40)
(ctrl *synth* :band-freq 10)

(free *synth*)
