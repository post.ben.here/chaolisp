(ql:quickload :3d-vectors)
(ql:quickload :clog)
(ql:quickload :cl-audio-programming)

(defpackage #:clog-user
  (:use #:cl #:clog #:3d-vectors #:audio)
  (:export start-tutorial))

(in-package :clog-user)

(defconstant canvas-width 600)
(defconstant canvas-height 400)


(defun on-new-window (body)
  (run-app body))

(defun run-app (body)
  (setf (title (html-document body)) "TooManyTubes")
  (set-on-click (create-button body :content "run")
		#'run-simulation)
  (set-on-click (create-button body :content "start server")
		#'init-audio)
  (set-on-click (create-button body :content "stop")
		#'stop-audio)
  (set-on-click (create-button body :content "quit-server")
		#'quit-audio)
  (let* ((canvas (create-canvas body :width canvas-width :height canvas-height))
	 (cx     (create-context2d canvas))
         (simulation (new-universe)))
    (set-border canvas :thin :solid :black)
    (loop
      (unless (validp body) (return))
      (if (connection-data-item body "isRunning")
          (progn
            (calc-update simulation)
            (render simulation cx)))
      (sleep 0.3))))


(defun start-app ()
  (initialize #'on-new-window))


(defun stop-app ()
  (shutdown))


(defun init-audio (obj)
  (audio::init))


(defun quit-audio (obj)
  (audio::quit))


(defun stop-audio (obj)
  (audio::stop))


(defun run-simulation (obj)
  (if (connection-data-item obj "isRunning")
    (progn
      (setf (connection-data-item obj "isRunning") nil)
      (setf (text obj) "run"))
    (progn
      (setf (connection-data-item obj "isRunning") t)
      (setf (text obj) "pause"))))


(defun new-universe ()
  (let* ((m 1.0)
         (r 1.0)
         (ctx (make-instance
               'universe
               :orbs
               (list
                (make-instance 'orb
                 :pos (vec (* 4.5 r) 0 0)
                 :mass m
                 :radius r
                 :color "red")
                (make-instance 'orb
                 :pos (vec (* -1.5 r) 0 0)
                 :mass m
                 :radius r
                 :color "yellow")
                (make-instance 'orb
                 :pos (vec (* 25.0 r) 0 0)
                 :mass (/ m 1000.0)
                 :radius (* 0.5 r)
                 :color "blue")))))
    (destructuring-bind (r1 r2 r3) (orbs ctx)
      (setf (orb-pm r1) (v* (orb-mass r1) (vec 0 2 0)))
      (setf (orb-pm r3) (v* (orb-mass r3) (vec 0 3 0)))
      ;; set momentum of 2nd body so momentum of system is 0
      (setf (orb-pm r2) (v* -1.0 (v+ (orb-pm r1) (orb-pm r3)))))
    ctx))


(defclass orb ()
  ((pos
    :initarg :pos
    :initform (vec 0 0 0)
    :accessor orb-pos)
   (mass
    :initarg :mass
    :initform 1.0
    :accessor orb-mass)
   (radius
    :initarg :radius
    :initform 1.0
    :accessor orb-radius)
   (color
    :initarg :color
    :initform "black"
    :accessor orb-color)
   (pm  ;; momentum
    :initarg :pm
    :initform (vec 0 0 0)
    :accessor orb-pm)))


(defclass universe ()
  ((g
    :initarg :g
    :initform 100.0
    :accessor g)
   (dt
    :initarg :dt
    :initform 0.1
    :accessor dt)
   (clock
    :initarg :clock
    :initform 0.0
    :accessor clock)
   (orbs
    :initarg :orbs
    :initform '()
    :accessor orbs)))


(defun calc-update (ctx)
  (let* ((r1 (first (orbs ctx)))
         (r2 (second (orbs ctx)))
         (r3 (third (orbs ctx)))
         (g (g ctx))
         (dt (dt ctx))
         (f12 (grav-force g r1 r2))
         (f21 (v* -1.0 f12))
         (f13 (grav-force g r1 r3))
         (f23 (grav-force g r2 r3)))

    (setf (orb-pm r1) (v+ (orb-pm r1) (v* (v- f21 f13) dt)))
    (setf (orb-pm r2) (v+ (orb-pm r2) (v* (v- f12 f23) dt)))
    (setf (orb-pm r3) (v+ (orb-pm r3) (v* (v+ f13 f23) dt)))

    (setf (orb-pos r1) (v+ (orb-pos r1) (v* (orb-pm r1) (/ dt (orb-mass r1)))))
    (setf (orb-pos r2) (v+ (orb-pos r2) (v* (orb-pm r2) (/ dt (orb-mass r2)))))
    (setf (orb-pos r3) (v+ (orb-pos r3) (v* (orb-pm r3) (/ dt (orb-mass r3)))))

    (setf (clock ctx) (+ (clock ctx) dt))
    ctx))


(defun grav-force (g body1 body2)
  (let ((vdiff (v- (orb-pos body2) (orb-pos body1)))
        (c1 (* -1.0 g (orb-mass body1) (orb-mass body2))))
    (v/ (v* c1 (vunit vdiff)) (expt (v2norm vdiff) 2))))


(defmethod render (obj cx)
  (format t "Unknown render obj ~a" obj))


(defmethod render ((obj universe) cx)
  (destructuring-bind (r1 r2 r3) (orbs obj)
    (clear-rect cx 0 0 canvas-width canvas-height)
    (render r1 cx)
    (render r2 cx)
    (render r3 cx)))


(defmethod render ((obj orb) cx)
  (let* ((color (read-from-string (orb-color obj)))
         (radius (ceiling (* 20 (orb-radius obj))))
         (pos (orb-pos obj))
         (x (round (translate (vx3 pos) -40.0 40.0 0 canvas-width)))
         (y (round (translate (vy3 pos) -40.0 40.0 0 canvas-height))))
    (fill-style cx color)
    (begin-path cx)
    (ellipse cx x y radius radius 0 0 6.283)
    (path-stroke cx)
    (path-fill cx)))


(defun translate (value left-min left-max right-min right-max)
  (let* ((left-span (- left-max left-min))
         (right-span (- right-max right-min))
         (value-scaled (/ (- value left-min) left-span)))
    (+ right-min (* value-scaled right-span))))
